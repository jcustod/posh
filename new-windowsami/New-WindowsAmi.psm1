# Author: Jerameel Custodio
# Initial version: v0.01.2017.07.16
# Current version: v0.01.2017.07.19c
# Last updated: 2017/07/19

# Check to see if instance is stopped, if not, stop it
Function Prepare-EC2InstanceState {
  [CmdletBinding(SupportsShouldProcess = $True)]
  Param (
    [Parameter(Mandatory=$True)][string]$InstanceId,
    [Parameter(Mandatory=$True)][string]$Region
  )

  # Set this as a string to pass to Invoke-Expression so the instance state is updated per invocation
  $Instance = "Get-EC2Instance -InstanceId $InstanceId -Region $Region"

  Write-Output "$(Get-Date -Format T): Checking instance state."

  $Loop = 0
  Try {
    $InstanceState = ((Invoke-Expression $Instance).Instances.State.Name.Value)
    While (($InstanceState -ne "stopped") -and ($Loop -le 10)) {
      If (($InstanceState -eq "stopping") -or ($InstanceState -eq "shutting-down") -or ($InstanceState -eq "pending")) {
        Write-Verbose "$(Get-Date -Format T): $($Loop * 30) seconds have elapsed."
        Write-Output "$(Get-Date -Format T): Instance is currently $InstanceState. Waiting 30 seconds."
        $Loop++
        Start-Sleep -Seconds 30
        # Update instancestate
        $InstanceState = ((Invoke-Expression $Instance).Instances.State.Name.Value)
      }
      ElseIf ($InstanceState -eq "terminated") {
        Write-Error "$(Get-Date -Format T): Instance is currently $InstanceState. Exiting." -ErrorAction Stop
      }
      ElseIf ($InstanceState -eq "running") {
        Write-Output "$(Get-Date -Format T): Instance is currently $InstanceState. Attempting to stop."
        Stop-EC2Instance -InstanceId $InstanceId -Region $Region -Force
        # Update instancestate
        $InstanceState = ((Invoke-Expression $Instance).Instances.State.Name.Value)
      }
      Else {
        Write-Output "$(Get-Date -Format T): Instance is currently $InstanceState"
        Write-Error "$(Get-Date -Format T): Error: Unknown instance state. Exiting." -ErrorAction Stop
      }
    }
    If ($InstanceState -eq "stopped") {
      Write-Output "$(Get-Date -Format T): Instance is stopped."
    }
    Else {
      Write-Error "$(Get-Date -Format T): Error: Timeout. Instance is in state $InstanceState" -ErrorAction Stop
    }
  }
  Catch {
    Write-Output "$(Get-Date -Format T): Error: $($_.Exception.Message)"
  }
}

Function New-WindowsAmi {
  [CmdletBinding(SupportsShouldProcess = $True)]
  Param (
    [Parameter(Mandatory=$True)][string]$InstanceId,
    [Parameter(Mandatory=$True)][string]$Region,
    [switch]$Sysprep,
    [switch]$NoStart
  )

  BEGIN {
    # Get auth info from env variables and set
    Try {
      If ((Test-Path env:AWS_ACCESS_KEY_ID) -eq $True) {
        Write-Verbose "$(Get-Date -Format T): Verified AWS Credential environment variables exist, setting credentials."
        Set-AWSCredential -AccessKey $env:AWS_ACCESS_KEY_ID -SecretKey $env:AWS_SECRET_ACCESS_KEY -SessionToken $env:AWS_SESSION_TOKEN
      }
      Else {
        # If env variables don't exist, prompt for credentials
        Write-Verbose "$(Get-Date -Format T): AWS Credential environment variables not found."
        $env:AWS_ACCESS_KEY_ID = Read-Host "Enter AWS Access Key ID: "
        $env:AWS_SECRET_ACCESS_KEY = Read-Host "Enter AWS Secret Access Key: "
        $env:AWS_SESSION_TOKEN = Read-Host "Enter AWS Session Token: "
        Set-AWSCredential -AccessKey $env:AWS_ACCESS_KEY_ID -SecretKey $env:AWS_SECRET_ACCESS_KEY -SessionToken $env:AWS_SESSION_TOKEN
      }
    }
    Catch {
      Write-Output "$(Get-Date -Format T): Error: $($_.Exception.Message)"
    }
  }

  PROCESS {
    $Instance = "Get-EC2Instance -InstanceId $InstanceId -Region $Region"
    # Set InstanceProperties variable so we can query it for persistent properties to avoid invoking Get-EC2Instance unnecessarily
    $InstanceProperties = Invoke-Expression $Instance
    $InstanceSubnet = $InstanceProperties.Instances.SubnetId

    # Verify platform is Windows
    If ($InstanceProperties.Instances.Platform.Value -like "Windows") {
      Write-Output "$(Get-Date -Format T): Platform validated as Windows, continuing."
    }
    Else {
      Write-Error "$(Get-Date -Format T): Error: Unable to validate platform. Exiting." -ErrorAction Stop
    }
  
    # Prepare instance by stopping first if necessary
    Prepare-EC2InstanceState -InstanceId $InstanceId -Region $Region

    # Take initial/golden AMI
    Try {
      $EC2ImageName = "Initial-Ami-$(Get-Date -Format yyyyddMMHHmm)"
      $InitialAmiId = New-EC2Image -InstanceId $InstanceId -Name $EC2ImageName -Region $Region -Description "Initial AMI created by New-WindowsAmi"
      Write-Verbose "$(Get-Date -Format T): Taking initial AMI with a timeout of 30 minutes"
      $Loops = 0
      $AmiStatus = (Get-EC2Image -ImageId $InitialAmiId -Region $Region).State.Value
      If ($AmiStatus -eq "pending") {
        Do {
          Write-Verbose "$(Get-Date -Format T): AMI $InitialAmiId is currently $AmiStatus. Waiting 30 seconds."
          $Loops++
          Start-Sleep -Seconds 30
          $AmiStatus = (Get-EC2Image -ImageId $InitialAmiId -Region $Region).State.Value
          If ($Loops -eq 60) {
            Write-Error "$(Get-Date -Format T): Error: Reached 30 minute timeout. Exiting." -ErrorAction Stop
          }
          Else {
            Continue
          }
        }
        Until ($AmiStatus -eq "available")
      }
      ElseIf ($AmiStatus -eq "available") {
        Write-Verbose "$(Get-Date -Format T): Initial AMI is now available: $InitialAmiId. Continuing"
        Return
      }
    }
    Catch {
      Write-Output "$(Get-Date -Format T): Error: $($_.Exception.Message)"
    }
  

    # Create new IAM profile and role and attach AmazonEC2RoleforSSM managed policy
    Try {
      Write-Verbose "$(Get-Date -Format T): Creating new instance roles and profiles for SSM."
      $InstanceProfileName = "NewAmiInstanceProfile-$(Get-Date -Format yyyyddMMHHmm)"
      $IAMRoleName = "NewAmiInstanceProfile-$(Get-Date -Format yyyyddMMHHmm)"
      $AssumePolicy = @{
    'Version' = '2012-10-17'
    'Statement' = @{
        'Sid' = ''
        'Effect' = 'Allow'
        'Principal' = @{'Service' = 'ec2.amazonaws.com'}
        'Action' = 'sts:AssumeRole'
    }
}
      $AssumePolicy = ($AssumePolicy | ConvertTo-Json)
      New-IAMInstanceProfile -InstanceProfileName $InstanceProfileName
      New-IAMRole -RoleName $IAMRoleName -AssumeRolePolicyDocument $AssumePolicy
      Register-IAMRolePolicy -RoleName $IAMRoleName -PolicyArn arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM
      Add-IAMRoleToInstanceProfile -InstanceProfileName $InstanceProfileName -RoleName $IAMRoleName
      Write-Verbose "$(Get-Date -Format T): IAM profile created."
    }
    Catch {
      Write-Output "$(Get-Date -Format T): Error: $($_.Exception.Message)"
    }
    
    # Create new instance from initial AMI with SSM role
    Try {
      # $Tag = @{ Key="Name"; Value="NewAmiInstance-$(Get-Date -Format yyyyddMM)" }             commenting out tags for now as it's erroring out
      # $InstanceTag = New-Object Amazon.EC2.Model.TagSpecification
      # $InstanceTag.ResourceType = "instance"
      # $InstanceTag.Tags.Add($Tag)
      Write-Output "$(Get-Date -Format T): Attempting to create new instance"
      Start-Sleep -Seconds 30 # IAM assocation was failing, apparently it takes time for IAM role to be available, so adding sleep
      $NewInstance = (New-EC2Instance -ImageId $InitialAmiId -InstanceType t2.micro -Region $Region -SubnetId $InstanceSubnet -TagSpecification $InstanceTag -InstanceProfile_Name $InstanceProfileName)
      $NewInstanceId = $NewInstance.Instances.InstanceId
      Write-Output "$(Get-Date -Format T): Created instance $NewInstanceId"
    }
    Catch {
      Write-Output "$(Get-Date -Format T): Error: $($_.Exception.Message)"
    }

    # Wait for SSM ping status to return success
    $SSMStatus = (Get-SSMInstanceInformation -InstanceInformationFilterList @{Key="InstanceIds";ValueSet="$NewInstanceId"} -Region $Region).PingStatus.Value
    $Loops = 0
    If ($SSMStatus -notlike "Online") {
      Do {
        Write-Verbose "$(Get-Date -Format T): SSM ping status still pending. Waiting 30 seconds."
        $Loops++
        Start-Sleep -Seconds 30
        $SSMStatus = (Get-SSMInstanceInformation -InstanceInformationFilterList @{Key="InstanceIds";ValueSet="$NewInstanceId"} -Region $Region).PingStatus.Value
        If ($Loops -eq 20) {
          Write-Error "$(Get-Date -Format T): Error: Reached 10 minute timeout. Exiting." -ErrorAction Stop
        }
        Else {
          Continue
        }
      }
      Until ($SSMStatus -eq "Online")
    }
    ElseIf ($SSMStatus -eq "Online") {
    Write-Output "$(Get-Date -Format T): SSM status is online. Continuing"
    }


    # Run SSM commands:
    Try {
      $SSMCommand = "Remove-Item 'C:\cfn\*' -Exclude *.log -Recurse -Force`n`$configfile = 'C:\Program Files\Amazon\Ec2ConfigService\Settings\config.xml'`n`$config = (Get-Content `$configfile) -as [xml]`n(`$config.Ec2ConfigurationSettings.Plugins.Plugin | Where-Object Name -eq 'Ec2HandleUserData').State = 'Enabled'`n(`$config.Ec2ConfigurationSettings.Plugins.Plugin | Where-Object Name -eq 'Ec2SetPassword').State = 'Enabled'`n`$config.Save(`$configfile)`n. 'C:\Program Files\Amazon\Ec2ConfigService\Ec2Config.exe'"
      Write-Verbose "$(Get-Date -Format T): Attempting to run SSM command to sanitize instance"
      Send-SSMCommand -DocumentName "AWS-RunPowerShellScript" -Parameter @{commands = $SSMCommand} -Target @{Key="instanceids";Values=@("$NewInstanceId")} -Region $Region
      Start-Sleep -Seconds 120
    }
    Catch {
      Write-Output "$(Get-Date -Format T): Error: $($_.Exception.Message)"
    }
      # if $Sysprep=true, shutdown WITH sysprep - will add later
      # else shutdown WITHOUT sysprep (add later: check if domain joined)

    # Snap instance again for working AMI
    Prepare-EC2InstanceState -InstanceId $NewInstanceId -Region $Region
    # if $NoStart=true, don't start instance back up - later
    # else start instance
    Try {
      $FinalEC2ImageName = "Working-AMI-$(Get-Date -Format yyyyddMMHHmm)"
      $FinalAmiId = New-EC2Image -InstanceId $NewInstanceId -Name $FinalEC2ImageName -Region $Region -Description "Working AMI created by New-WindowsAmi"
      Write-Verbose "$(Get-Date -Format T): Taking final AMI with a timeout of 30 minutes"
      $Loops = 0
      $NewAmiStatus = (Get-EC2Image -ImageId $FinalAmiId -Region $Region).State.Value
      If ($NewAmiStatus -eq "pending") {
        Do {
          Write-Verbose "$(Get-Date -Format T): AMI $FinalAmiId is currently $NewAmiStatus. Waiting 30 seconds."
          $Loops++
          Start-Sleep -Seconds 30
          $NewAmiStatus = (Get-EC2Image -ImageId $FinalAmiId -Region $Region).State.Value
          If ($Loops -eq 60) {
            Write-Error "$(Get-Date -Format T): Error: Reached 30 minute timeout. Exiting." -ErrorAction Stop
          }
          Else {
            Continue
          }
        }
        Until ($NewAmiStatus -eq "available")
      }
      ElseIf ($NewAmiStatus -eq "available") {
        Return $FinalAmiId
      }
    }
    Catch {
      Write-Output "$(Get-Date -Format T): Error: $($_.Exception.Message)"
    }
    Write-Output "$(Get-Date -Format T): Final AMI is now available: $FinalAmiId."
  }

  END {
    # TODO: cleanup - remove temp env variables, clean up created resources, etc
  }
}