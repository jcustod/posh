###################################################
# JC-Import-Certificate.ps1                       #
# v1.0 - 2016-08-04                               #
# Jerameel Custodio                               #
###################################################


# Function to open file dialog and select pfx file to import
Function Get-FileName
{   
    [void][System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms')

    $openfile = New-Object System.Windows.Forms.OpenFileDialog
    $openfile.filter = 'Personal Information Exchange (*.pfx)| *.pfx'
    $openfile.initialDirectory = 'C:\'
    $openfile.ShowDialog() | Out-Null
    $openfile.filename
}

$certlocation = Get-FileName

# Get password for pfx file
$securepassword = Read-Host -Prompt 'Enter PFX password' -AsSecureString

# Import PFX to local machine personal store

Try {
	If ($securepassword -eq '') {
	Import-PfxCertificate -CertStoreLocation Cert:\LocalMachine\My -FilePath $certlocation -Exportable
	Write-Host ''
	Write-Host 'Successfully imported certificate' $certlocation 'to' $env:computername -ForegroundColor green
	}
	Else {
	Import-PfxCertificate -CertStoreLocation Cert:\LocalMachine\My -FilePath $certlocation -Password $securepassword -Exportable
	Write-Host ''
	Write-Host 'Successfully imported certificate' $certlocation 'to' $env:computername -ForegroundColor green
	}
}
Catch {
$errormessage = $_.Exception.Message
Write-Host $errormessage -ForegroundColor Red
}
Finally
{
Write-Host ''
Write-Host 'List of certificates in local machine personal store:'
Write-Host '====================================================='
Get-ChildItem Cert:\LocalMachine\My | Select Subject, @{Name='Expires On'; Expression = {$_.NotAfter}}
Write-Host ''
}
Pause