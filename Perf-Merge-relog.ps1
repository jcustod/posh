# Merge all perfmon (.blg) log files in directory

$logpath = Read-Host -Prompt 'Enter Path to perfmon logs. This is recursive'
$outputpath = Read-Host -Prompt 'Enter output path and filename eg. c:\rs-pkgs\combined.blg'
$perflogs = Get-ChildItem -Path $logpath -Filter *.blg -Recurse | ForEach-Object -Process { $_.FullName }

relog $perflogs -f BIN -o $outputpath
